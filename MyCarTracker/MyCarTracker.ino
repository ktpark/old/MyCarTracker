//#include <Adafruit_GFX.h>    // Core graphics library
#include <Adafruit_ST7735.h> // Hardware-specific library
#include <SD.h>
#include <SPI.h>

#define TINY_GSM_MODEM_SIM800
#include <TinyGsmClient.h>

#include <NMEAGPS.h>
#include <EEPROM.h>

#define DIAL 0
#define BUTTON 2
#define GPS_SERIAL 3
#define TFT_LED 4
#define SD_PWR 5

#define SD_CS 48
#define TFT_CS 49
#define TFT_RST 0
#define TFT_DC 53

#define SerialGPS Serial2

Adafruit_ST7735 tft = Adafruit_ST7735(TFT_CS,  TFT_DC, TFT_RST);
TinyGsm modem(Serial1);
TinyGsmClient client(modem);

void copyright()
{
  tft.drawFastHLine(0, 148, 128, 0x630C);
  tft.fillRect(0, 149, 128, 11, 0x4208);

  tft.setTextColor(ST7735_YELLOW);
  tft.setCursor(2, 151);
  tft.setTextSize(1);
  tft.println(F("Kevin Park"));

  tft.setCursor(85, 151);
  tft.println(F("(C)2017"));
}

int unitAddress = 0;
bool unitSwitch = false;
void buttonPress()
{
  unitSwitch = !unitSwitch;
  EEPROM.update(unitAddress, unitSwitch);
}

void setup()
{
  pinMode(GPS_SERIAL, OUTPUT);
  pinMode(TFT_LED, OUTPUT);
  pinMode(SD_PWR, OUTPUT);

  analogWrite(TFT_LED, 255); //Power the screen
  
  digitalWrite(GPS_SERIAL, HIGH); //Allow GPS Serial Communication
  SerialGPS.begin(57600);
  while (!SerialGPS);
  SerialGPS.println(F("$PMTK251,115200*1F")); //Change baud rate
  SerialGPS.end();

  SerialGPS.begin( 115200 );
  while (!SerialGPS);
  SerialGPS.println(F("$PMTK314,0,1,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0*28")); //Only RMC, GGA
  SerialGPS.println(F("$PMTK220,1000*1F")); //1Hz

  //SerialGPS.println(F("$PMTK314,-1*04")); //RESET
  //SerialGPS.println(F("$PMTK104*37")); // FULL COLD RESTART

  Serial1.begin(115200);
  while (!Serial1);

  //Serial.begin(115200);
  
  digitalWrite(SD_PWR, HIGH);
  tft.initR(INITR_BLACKTAB);
  tft.fillScreen(ST7735_BLACK);
  copyright();
  
  if (!SD.begin(SD_CS)) {
    tft.setTextColor(ST7735_RED);
    tft.setTextSize(1);
    tft.setCursor(0, 0);
    tft.print(F("SD Card Initialization Failed!"));
    return;
  }

  if (!modem.init()) {
    tft.setTextColor(ST7735_RED);
    tft.setTextSize(1);
    tft.setCursor(0, 0);
    tft.print(F("Modem Initialization Failed!"));
    return;
  }

  pinMode(BUTTON, INPUT_PULLUP);
  unitSwitch = EEPROM.read(unitAddress);
  attachInterrupt(digitalPinToInterrupt(BUTTON), buttonPress, FALLING);
}

NMEAGPS gps;
gps_fix fix;

char buff[12];
int signalQuality = 0;
bool displaySwitch = false;
void gps_module()
{
  tft.fillRoundRect(1, 1, 62, 28, 3, ST7735_BLACK); //Clear Left Rect
  tft.drawRoundRect(0, 0, 64, 30, 5, 0xC618); //Left Rect
  tft.setTextSize(2);
  if (fix.status == fix.STATUS_STD || fix.status == fix.STATUS_DGPS)
    tft.setTextColor(ST7735_GREEN);
  else
    tft.setTextColor(ST7735_RED);
  if (displaySwitch)
  {
    tft.setCursor(14, 8);
    tft.print(F("GPS"));
  }
  else
  {
    sprintf(buff, "%02d", fix.satellites);
    tft.setCursor(8, 8);
    tft.print(buff);

    tft.setTextSize(1);
    tft.setCursor(34, 15);
    tft.print(F("sats"));
  }
}

void cell_module()
{
  tft.fillRoundRect(65, 1, 62, 28, 3, ST7735_BLACK); //Clear Right Rect
  tft.drawRoundRect(64, 0, 64, 30, 5, 0xC618); //Right Rect

  tft.setTextSize(2);
  if (signalQuality <= 73)
    tft.setTextColor(ST7735_GREEN);
  else if (signalQuality <= 83)
    tft.setTextColor(0xFFE0);
  else if (signalQuality <= 93)
    tft.setTextColor(0xFC00);
  else
    tft.setTextColor(ST7735_RED);
  if (displaySwitch)
  {
    tft.setCursor(74, 8);
    tft.print(F("Cell"));
  }
  else
  {
    if (signalQuality < 100)
      tft.setCursor(76, 8);
    else
      tft.setCursor(68, 8);
    tft.print(signalQuality);

    tft.setTextSize(1);
    tft.setCursor(105, 15);
    tft.print(F("dBm"));
  }
  displaySwitch = !displaySwitch;
}

void compass(float heading)
{
  int centerX = 109;
  int centerY = 70;
  int radius = 12;

  float course = 450 - heading;
  course = course / 180 * 3.14159265358979323846;

  float x = cos(course);
  float y = sin(course);

  float x1 = centerX - y * (radius / 3);
  float y1 = centerY + x * (radius / 3);
  float x2 = centerX + y * (radius / 3);
  float y2 = centerY - x * (radius / 3);

  tft.fillTriangle(x1, y1, x2, y2, centerX + x * radius, centerY + y * radius, ST7735_WHITE);
  tft.fillTriangle(x1, y1, x2, y2, centerX - x * radius, centerY - y * radius, ST7735_RED);
  tft.drawCircle(centerX, centerY, radius + 1, 0xC618);

  if (heading < 100)
    tft.setCursor(centerX - 11, centerY + 16);
  else
    tft.setCursor(centerX - 14, centerY + 16);
  tft.print(String(heading, 1));
}

void date_time()
{
  tft.setTextColor(ST7735_WHITE);
  tft.setTextSize(2);
  tft.setCursor(9, 37);
  tft.print(F("UTC:"));

  tft.setTextSize(1);
  tft.fillRect(61, 35, 59, 17, ST7735_BLACK);

  if (fix.valid.date)
  {
    sprintf(buff, "%02d/%02d/20%02d", fix.dateTime.month, fix.dateTime.date, fix.dateTime.year);
    tft.setCursor(61, 35);
    tft.print(buff);
  }

  if (fix.valid.time)
  {
    sprintf(buff, "%02d:%02d:%02d", fix.dateTime.hours, fix.dateTime.minutes, fix.dateTime.seconds);
    tft.setCursor(67, 45);
    tft.print(buff);
  }
}

void heading()
{
  tft.fillRect(95, 58, 30, 36, ST7735_BLACK);
  if (fix.valid.heading)
    compass(fix.heading());
}

void speed()
{
  char format[] = "%5s %4s"; 
  tft.setTextSize(1);
  tft.setTextColor(ST7735_WHITE);
  tft.setCursor(5, 58);
  tft.print(F("Speed:"));

  tft.fillRect(28, 67, 60, 8, ST7735_BLACK);
  if (fix.valid.speed)
  {
    char temp[5];
    tft.setCursor(28, 67);
    if (unitSwitch)
    {
      dtostrf(fix.speed_mph(), 5, 1, temp);
      sprintf(buff, format, temp, "mph");
    }
    else
    {
      dtostrf(fix.speed_kph(), 5, 1, temp);
      sprintf(buff, format, temp, "km/h");
    }
    tft.print(buff);
  }
}

void altitude()
{
  char format[] = "%7s %2s"; 
  tft.setTextSize(1);
  tft.setTextColor(ST7735_WHITE);
  tft.setCursor(5, 77);
  tft.print(F("Altitude:"));

  tft.fillRect(28, 86, 60, 8, ST7735_BLACK);
  if (fix.valid.altitude)
  {
    char temp[7];
    float alt = fix.altitude();
    tft.setCursor(28, 86);
    if (unitSwitch)
    {
      alt *= 3.28084;
      dtostrf(alt, 7, 1, temp);
      sprintf(buff, format, temp, "ft");
    }
    else
    {
      dtostrf(alt, 6, 1, temp);
      sprintf(buff, format, temp, "m");
    }
    tft.print(buff);
  }
}

void location()
{
  tft.setTextSize(1);
  tft.setTextColor(ST7735_WHITE);
  tft.setCursor(0, 98);
  tft.print(F("Latitude:"));
  tft.setCursor(0, 107);
  tft.print(F("Longitude:"));

  tft.fillRect(62, 98, 66, 16, ST7735_BLACK);
  if (fix.valid.location)
  {
    dtostrf(fix.latitude(), 11, 6, buff);
    tft.setCursor(62, 98);
    tft.print(buff);

    dtostrf(fix.longitude(), 11, 6, buff);
    tft.setCursor(62, 107);
    tft.print(buff);
  }
}

#define PACKET_SIZE 22
struct INFORMATION_t
{
  unsigned long unix; //Unix time
  long lat; //1.0e-7 degrees
  long lon; //1.0e-7 degrees
  long alt; //centi-meters
  unsigned long spd; //milli-knots
  unsigned int hdg; //centi-degrees
};

typedef union
{
  INFORMATION_t info;
  uint8_t bytes[PACKET_SIZE];
} DATA_UNION_t;

const char filename[] = "gps.dat";
File dataFile;
void save()
{
  INFORMATION_t info;
  info.unix = fix.dateTime + 946684800; //Add Saturday, January 1, 2000 12:00:00 AM
  info.lat = fix.latitudeL();
  info.lon = fix.longitudeL();
  info.alt = fix.altitude_cm();
  info.spd = fix.speed_mkn();
  info.hdg = fix.heading_cd();

  DATA_UNION_t packet;
  packet.info = info;

  cli(); //Disable interrupt
  dataFile = SD.open(filename, FILE_WRITE);
  if (dataFile) {
    dataFile.write(packet.bytes, PACKET_SIZE);
    dataFile.close();
  }
  sei(); //Re-enable interrupt
}


#define LONG_UNION_SIZE 4
typedef union
{
  long unsigned num;
  uint8_t bytes[LONG_UNION_SIZE];
} LONG_UNION_t;

const char server[] = "linux.tekhak.com";
const int port = 49152;

#define CHUNK_SIZE 1024
bool hasSent = false;
void transmit()
{
  hasSent = true;
  tft.fillRect(0, 118, 128, 148-118, 0x2104);
  
  tft.setTextColor(ST7735_CYAN);
  tft.setTextSize(1);
  tft.setCursor(0, 122); //Up to 3 lines

  //tft.print(F("File, Network, GPRS, Server connection failed"));
  
  dataFile = SD.open(filename);
  
  tft.print(F("File."));
  if (!dataFile) {
    tft.setTextColor(ST7735_MAGENTA);
    tft.print(F("does not exist"));
    return;
  }
  tft.print(F("Network."));
  if (!modem.waitForNetwork(15000L)) {
    tft.setTextColor(ST7735_MAGENTA);
    tft.print(F("not found"));
    return;
  }
  tft.print(F("GPRS."));
  if (!modem.gprsConnect("h2g2", "", "")) {
    tft.setTextColor(ST7735_MAGENTA);
    tft.print(F("link unsuccessful"));
    return;
  }
  tft.print(F("Server."));
  if (!client.connect(server, port)) {
    tft.setTextColor(ST7735_MAGENTA);
    tft.print(F("connection failed"));
    return;
  }
  tft.print(F("Sending."));
  //client.setTimeout(5000);

  // 1: Size of data
  LONG_UNION_t dataSize;
  dataSize.num = dataFile.size();
  client.write(dataSize.bytes, LONG_UNION_SIZE); 

  // 2: Send data
  while (dataFile.available()) {
    uint8_t temp[CHUNK_SIZE];
    unsigned int tempSize = dataFile.readBytes(temp, CHUNK_SIZE);

    int sentSize = 0;
    do {
      sentSize = client.write(temp, tempSize);
    } while (sentSize != tempSize);
  }
  client.flush();
  dataFile.close();
  tft.print(F("Checking."));

  LONG_UNION_t checkSize;
  checkSize.num = 0;
  unsigned long timeout = millis();
  unsigned long bytesReceived = 0;
  while (bytesReceived < 4 && client.connected() && (millis() - timeout < 5000L) ) {
    while (client.available()) {
      uint8_t c = client.read();
      checkSize.bytes[bytesReceived] = c;
      bytesReceived += 1;
      timeout = millis();
    }
  }
  
  client.stop();
  modem.gprsDisconnect();

  if(dataSize.num != checkSize.num)
  {
    tft.setTextColor(ST7735_MAGENTA);
    //tft.print(String(dataSize.num) + " " + String(checkSize.num));
    tft.print(F("\nVerification failed"));
  }
  else
  {
    tft.setTextColor(ST7735_CYAN);
    tft.print(F("\nSuccessfully sent"));
    SD.remove(filename);
  }
}

#define TIMER_MAX 5
int timer = TIMER_MAX;
void loop()
{
  analogWrite(TFT_LED, analogRead(DIAL) / 4);
  signalQuality = 109 - (modem.getSignalQuality() - 2) * 2;
  
  while (gps.available( SerialGPS )) {
    cli(); //Disable interrupt
    fix = gps.read();
    sei(); //Re-enable interrupt

    gps_module();
    cell_module();
    date_time();
    heading();
    speed();
    altitude();
    location();

    if (fix.valid.date && fix.valid.time && fix.valid.heading && fix.valid.location && fix.valid.altitude && fix.valid.speed) {
      save();
      
      float spd = fix.speed_kph();
      tft.drawFastHLine(0, 117, 128, 0x630C);
      if (spd < 1 && timer > 0)
      {
        tft.fillRect(0, 118, 128, 148-118, 0x2104);
        timer--;

        if(timer > 0)
        {
          tft.setTextColor(ST7735_CYAN);
          
          tft.setTextSize(1);
          tft.setCursor(7, 123);
          tft.print(F("Sending"));
          tft.setTextSize(1);
          tft.setCursor(7, 136);
          tft.print(F("Data in"));
          
          tft.setTextSize(3);
          tft.setCursor(57, 123);
          tft.print(timer);
  
          tft.setTextSize(1);
          tft.setCursor(80, 136);
          tft.print(F("seconds"));
        }
      }
      else if (spd > 1 && timer != TIMER_MAX)
      {
        tft.fillRect(0, 118, 128, 148-118, 0x2104);
        hasSent = false;
        timer = TIMER_MAX;
      }
      
      if (timer != 0 || hasSent)
        return;
      transmit();
    }
  }
}
