Description
===========

MyCarTracker is a easy way to log and track your car's location.

Installation
============

MyCarTracker follows the standard Python installation procedure:

Install necessary dependencies
```bash
$ sudo pip install -r requirements.txt
```

Insert your Telegram bot token
```bash
$ nano config/telegram.json
```

Run!
```bash
$ python main.py
```

Pictures
============
![Front](https://i.gyazo.com/ed086c57cede4fcdd7ce83f2bd554fb9.jpg)
![Back](https://i.gyazo.com/0c92c6db4a6f94d5c852ad46e8078927.jpg)