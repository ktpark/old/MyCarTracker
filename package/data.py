import os, threading
from packet import Packet

root_path = os.path.dirname(os.path.realpath(__file__))
root_path = os.path.join(os.path.join(root_path, '..'), 'data')
lock = threading.Lock()

def setup():
    if not os.path.exists(root_path):
        os.makedirs(root_path)

def directory(info):
    dir = os.path.join(os.path.join(os.path.join(root_path, str(info.time.year)), str(info.time.month)), str(info.time.day))
    if not os.path.exists(dir):
        os.makedirs(dir)
    return dir
    
def write(data):
    info = Packet(data)
    if not info.verify():
        return
    path = os.path.join(directory(info), str(info.time.hour))
    lock.acquire()
    try:
        with open(path, "a+") as myfile:
            myfile.write('{} {} {} {} {} {}\n'.format(info.unix, info.lat, info.lon, info.alt, info.spd, info.hdg))
    finally:
        lock.release()

def read_last():
    path = root_path
    for x in range(0, 4):
        dirs = os.listdir(path)
        largest = 0
        for d in dirs:
            num = int(d)
            if num > largest:
                largest = num
        path = os.path.join(path, str(largest))
    print path
    lock.acquire()
    try:
        with open(path, "r") as dataFile:
            last_line = dataFile.readlines()[-1]
        info = Packet()
        info.parse(last_line)
    finally:
        lock.release()
    return info
