import data
import json, os, threading, socket, struct

def load_config():
    global port
    path = os.path.dirname(os.path.realpath(__file__))
    path = os.path.join(os.path.join(os.path.join(path, '..'), 'config'), 'server.json')
	
    with open(path) as data_file:    
        data = json.load(data_file)
    port = data['port']

def start():
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    server_address = ('', port)
    sock.bind(server_address)

    print 'starting up on %s port %s' % sock.getsockname()
    sock.listen(5)

    while True:
        print 'waiting for a connection'
        client, address = sock.accept()
        client.settimeout(10)
        threading.Thread(target = connection,args = (client,address)).start()

def connection(client, address):
    print 'client connected:', address
    content_length = client.recv(4, socket.MSG_WAITALL)
    content_length = struct.unpack('I', content_length)[0]
    print  "Length: " + str(content_length)
    
    raw_data = ''
    bytes_received = 0
    try:
        while True:
            temp = client.recv(1024)
            if not temp:
                break
            raw_data += temp 
            bytes_received += len(temp)
            if bytes_received >= content_length:
                break
    except socket.timeout:
        print  "Timeout:", address
		print  "Only received: " + str(bytes_received)
        client.close()
        return
    print "Received: " + str(bytes_received)
    
    received_size = struct.pack('I', bytes_received)
    client.send(received_size)
    client.close()
    
    if content_length != bytes_received:
        return
    print  "All data received"
    
    for x in xrange(0, bytes_received, 22):
        if x + 22 > bytes_received:
            break
        data.write(raw_data[x:x+22])
