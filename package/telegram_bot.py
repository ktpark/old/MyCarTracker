from telegram.ext import (Updater, CommandHandler, MessageHandler, Filters)
from datetime import datetime
import os, json
import data

def load_config():
    global token
    path = os.path.dirname(os.path.realpath(__file__))
    path = os.path.join(os.path.join(os.path.join(path, '..'), 'config'), 'telegram.json')

    with open(path) as data_file:
        data = json.load(data_file)
    token = data['token']

def verify(user_id):
    if user_id == 446407858:
        return True
    return False

def start(bot, update):
    name = update.message.from_user.first_name + ' ' + update.message.from_user.last_name
    update.message.reply_text('Welcome {}!'.format(name))
    if not verify(update.message.from_user.id):
        update.message.reply_text('However, you are unauthorized.')

def location(bot, update):
    if not verify(update.message.from_user.id):
        return
    info = data.read_last()
    elapsed = datetime.utcnow() - info.time
    message = "Last seen {} days {} hours {} minutes ago\n".format(elapsed.days, elapsed.seconds//3600, (elapsed.seconds//60)%60)
    update.message.reply_text(message)
    update.message.reply_location(info.latitude(), info.longitude())
    update.message.reply_text('Altitude: {0:.1f}m, {1:.1f}ft'.format(info.altitude_m(), info.altitude_ft()))

def travel(bot, update):
    if not verify(update.message.from_user.id):
        return
    update.message.reply_text('test')


def init():
    updater = Updater(token)

    dispatcher = updater.dispatcher

    dispatcher.add_handler(CommandHandler('start', start))
    dispatcher.add_handler(CommandHandler('location', location))
    dispatcher.add_handler(CommandHandler('travel', travel))

    updater.start_polling()
    updater.idle()
