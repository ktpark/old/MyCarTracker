from packet import Packet
import struct

time = 1102006222
lat = 892006223
lon = 1302006225
alt = 1402006226
spd = 1502006229
hdg = -16000

data = struct.pack('L', time)
data += struct.pack('i', lat)
data += struct.pack('i', lon)
data += struct.pack('i', alt)
data += struct.pack('i', spd)
data += struct.pack('h', hdg)

wow = Packet(data)

print wow.unix
print wow.lat
print wow.lon
print wow.alt
print wow.spd
print wow.hdg
print
print wow.latitude()
print wow.longitude()
print wow.altitude_m()
print wow.speed_kn()
print wow.heading_d()
print
print wow.verify()