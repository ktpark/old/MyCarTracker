import struct, math
from datetime import datetime

class Packet:
    def __init__(self, data = None):
        if data is None:
            return
            
        self.unix = struct.unpack('I', data[ 0: 4])[0]
        self.lat = struct.unpack('i', data[ 4: 8])[0]
        self.lon = struct.unpack('i', data[ 8:12])[0]
        self.alt = struct.unpack('i', data[12:16])[0]
        self.spd = struct.unpack('I', data[16:20])[0]
        self.hdg = struct.unpack('H', data[20:22])[0]
        
        self.time = datetime.utcfromtimestamp(self.unix)
        
    def parse(self, line):
        parts = line.split()
        self.unix = int(parts[0])
        self.lat = int(parts[1])
        self.lon = int(parts[2])
        self.alt = int(parts[3])
        self.spd = int(parts[4])
        self.hdg = int(parts[5])
        
        self.time = datetime.utcfromtimestamp(self.unix)
        
    def verify(self):
        if math.fabs(self.latitude()) <= 90 and math.fabs(self.longitude()) <= 180 and self.heading_d() < 360:
            return True
        return False
        
    def latitude(self):
        return self.lat * 1.0e-7
        
    def longitude(self):
        return self.lon * 1.0e-7
        
    def altitude_m(self):
        return self.alt * 1.0e-2
        
    def altitude_ft(self):
        return self.altitude_m() * 3.28084
        
    def speed_kn(self):
        return self.spd * 1.0e-3
        
    def speed_kph(self):
        return self.speed_kn() * 1.852
        
    def speed_mph(self):
        return self.speed_kn() * 1.1507794
        
    def heading_d(self):
        return self.hdg * 1.0e-2
