from package import telegram_bot, server, data
import threading, os

pid = os.getpid()
print "PID: " + str(pid)

data.setup()
telegram_bot.load_config()
server.load_config()

threading.Thread(target = server.start,args = ()).start()
telegram_bot.init()

os.kill(pid,9)
